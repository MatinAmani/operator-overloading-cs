﻿using System;
using System.Globalization;

class Complex
{
    public double a, b;
    public Complex(double x, double y)
    {
        a = x;
        b = y;
    }
    public void Print()
    {
        Console.WriteLine("{0} + {1}i", a, b);
    }
    public static Complex operator +(Complex A, Complex B)
    {
        Complex C = new Complex(0, 0);
        C.a = A.a + B.a;
        C.b = A.b + B.b;
        return (C);
    }
    public static Complex operator -(Complex A, Complex B)
    {
        Complex C = new Complex(0, 0);
        C.a = A.a - B.a;
        C.b = A.b - B.b;
        return (C);
    }
    public static Complex operator -(Complex A)
    {
        //Conjugate of Complex
        Complex C = new Complex(0, 0);
        C.a = A.a;
        C.b = -1 * (A.b);
        return (C);
    }
    public static Complex operator *(Complex A, Complex B)
    {
        Complex C = new Complex(0, 0);
        C.a = (A.a * B.a) - (A.b * B.b);
        C.b = (A.a * B.b) + (B.a * A.b);
        return (C);
    }
    public static Complex operator /(Complex A, Complex B)
    {
        Complex C = new Complex(0, 0);
        C = (A * (-B));
        C.a /= !B;
        C.b /= !B;
        return (C);
    }
    public static double operator !(Complex A)
    {
        //Squared Size of Complex
        return ((A.a * A.a) + (A.b * A.b));
    }
}

class Matrix
{
    public int n;
    public double[,] a;
    public void ReadMatrix()
    {
        Console.Write("Enter matrix degree: ");
        n = int.Parse(Console.ReadLine());
        a = new double[n, n];
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                Console.Write("[{0}, {1}]: ", i + 1, j + 1);
                a[i, j] = double.Parse(Console.ReadLine());
            }
            Console.WriteLine();
        }
        Console.Clear();
    }
    public void PrintMatrix()
    {
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                Console.Write("{0}\t", a[i, j]);
            }
            Console.WriteLine();
        }
    }
    public static Matrix operator +(Matrix A, Matrix B)
    {
        Matrix C = new Matrix();
        if (A.n != B.n)
        {
            Console.WriteLine("Summition Not Possible!");
            return (C);
        }
        else
        {
            C.n = A.n;
            for (int i = 0; i < A.n; i++)
            {
                for (int j = 0; j < A.n; j++)
                {
                    C.a[i, j] = A.a[i, j] + B.a[i, j];
                }
            }
            return (C);
        }
    }
    public static Matrix operator +(Matrix A, int B)
    {
        Matrix C = new Matrix();
        C.n = A.n;
        for (int i = 0; i < A.n; i++)
        {
            for (int j = 0; j < A.n; j++)
            {
                C.a[i, j] = A.a[i, j] + B;
            }
        }
        return (C);
    }
    public static Matrix operator -(Matrix A, Matrix B)
    {
        Matrix C = new Matrix();
        if (A.n != B.n)
        {
            Console.WriteLine("Subtraction Not Possible!");
            return (C);
        }
        else
        {
            C.n = A.n;
            for (int i = 0; i < A.n; i++)
            {
                for (int j = 0; j < A.n; j++)
                {
                    C.a[i, j] = A.a[i, j] - B.a[i, j];
                }
            }
            return (C);
        }
    }
    public static Matrix operator -(Matrix A, int B)
    {
        Matrix C = new Matrix();
        C.n = A.n;
        for (int i = 0; i < A.n; i++)
        {
            for (int j = 0; j < A.n; j++)
            {
                C.a[i, j] = A.a[i, j] - B;
            }
        }
        return (C);
    }
    public static Matrix operator *(Matrix A, Matrix B)
    {
        Matrix C = new Matrix();
        if (A.n != B.n)
        {
            Console.WriteLine("Multiplication Not Possible!");
            return (C);
        }
        else
        {
            C.n = A.n;
            for (int i = 0; i < A.n; i++)
            {
                for (int j = 0; j < A.n; j++)
                {
                    C.a[i, j] = 0;
                }
            }
            for (int i = 0; i < A.n; i++)
            {
                for (int j = 0; j < A.n; j++)
                {
                    for (int k = 0; k < A.n; k++)
                    {
                        C.a[i, j] += A.a[i, k] * B.a[k, j];
                    }
                }
            }
            return (C);
        }
    }
    public static Matrix operator *(Matrix A, int B)
    {
        Matrix C = new Matrix();
        C.n = A.n;
        for (int i = 0; i < A.n; i++)
        {
            for (int j = 0; j < A.n; j++)
            {
                C.a[i, j] = A.a[i, j] * B;
            }
        }
        return (C);
    }
    public static Matrix operator !(Matrix A)
    {
        //Transpose of Matrix
        Matrix C = new Matrix();
        C.n = A.n;
        for (int i = 0; i < A.n; i++)
        {
            for (int j = 0; j < A.n; j++)
            {
                C.a[i, j] = A.a[j, i];
            }
        }
        return (C);
    }
}
class Vector
{
    public double a, b;
    public Vector(double x,double y)
    {
        a = x;
        b = y;
    }
    public void Print()
    {
        Console.WriteLine("({0}i + {1}j)", a, b);
    }
    public static Vector operator +(Vector A,Vector B)
    {
        Vector C = new Vector(0, 0);
        C.a = A.a + B.a;
        C.b = A.b + B.b;
        return (C);
    }
    public static Vector operator -(Vector A,Vector B)
    {
        Vector C = new Vector(0, 0);
        C.a = A.a - B.a;
        C.b = A.b - B.b;
        return (C);
    }
    public static Vector operator -(Vector A)
    {
        Vector C = new Vector(0, 0);
        C.a = (-1) * A.a;
        C.b = (-1) * A.b;
        return (C);
    }
    public static double operator *(Vector A,Vector B)
    {
        //Dot Product
        return (A.a * B.a) + (A.b * B.b);
    }
    public static double operator !(Vector A)
    {
        //Squared Size of Vector
        return ((A.a * A.a) + (A.b * A.b));
    }
}
class Program
{
    public static void Main()
    {
        //Matrix a = new Matrix();
        //Matrix b = new Matrix();
        //int d = 2;
        //a.ReadMatrix();
        //b.ReadMatrix();
        //Matrix c = new Matrix();
        //c = a * d;
        //c.PrintMatrix();
        Console.ReadKey();
    }
}